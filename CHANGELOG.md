# Changelog

# [0.2.0](https://gitlab.com/programmer-assistant/addon-prometheus/compare/v0.1.11...v0.2.0) (2022-02-06)


### :arrow_up:

* Update Prometheus to 2.33.1 ([e70022f](https://gitlab.com/programmer-assistant/addon-prometheus/commit/e70022f35688902e9b6e31baf87ae9e2ebb1b613))

## [0.1.11](https://gitlab.com/programmer-assistant/addon-prometheus/compare/v0.1.10...v0.1.11) (2021-09-19)


### :green_heart:

* Migrate to shared docker pipeline ([36a264c](https://gitlab.com/programmer-assistant/addon-prometheus/commit/36a264cabfe862c52f5f444535bbee34f66f029f))

## [0.1.10](https://gitlab.com/programmer-assistant/addon-prometheus/compare/v0.1.9...v0.1.10) (2021-09-19)


### :green_heart:

* Fix publishing repository ([9319f7a](https://gitlab.com/programmer-assistant/addon-prometheus/commit/9319f7a362b50c6086ec6434c32025e9e37393a1))

## [0.1.9](https://gitlab.com/programmer-assistant/addon-prometheus/compare/v0.1.8...v0.1.9) (2021-09-19)


### :green_heart:

* Fix publishing repository ([3b955f6](https://gitlab.com/programmer-assistant/addon-prometheus/commit/3b955f69b9d213133fe6b404c8ab72f060d9dda5))

## [0.1.8](https://gitlab.com/programmer-assistant/addon-prometheus/compare/v0.1.7...v0.1.8) (2021-09-18)


### :green_heart:

* Add auto registry update ([ab1ba48](https://gitlab.com/programmer-assistant/addon-prometheus/commit/ab1ba4843d53d4146a4c42b4231f5f6260630d24))

### :rotating_light:

* Fix linter issues ([837209f](https://gitlab.com/programmer-assistant/addon-prometheus/commit/837209f618d0dfb0472279d32ccbb385e7c247c0))

## [0.1.7](https://gitlab.com/programmer-assistant/addon-prometheus/compare/v0.1.6...v0.1.7) (2021-09-18)


### :green_heart:

* Fix release CI ([055665c](https://gitlab.com/programmer-assistant/addon-prometheus/commit/055665cf8a52e5b479391c4f832918e883232939))

## [0.1.6](https://gitlab.com/programmer-assistant/addon-prometheus/compare/v0.1.5...v0.1.6) (2021-09-18)


### :green_heart:

* Add semantic release ([dfa095c](https://gitlab.com/programmer-assistant/addon-prometheus/commit/dfa095cb8547d957295218d3d567fe7f63e95a60))
* Fix semantic installation ([176b567](https://gitlab.com/programmer-assistant/addon-prometheus/commit/176b56753a276680906d6d896cf05e4663e906b9))
* Remove snyk monitor and tags from release ([459b798](https://gitlab.com/programmer-assistant/addon-prometheus/commit/459b7981f70d14691846efa2389d8c7935f90015))
* Run release right after deploy ([9318f32](https://gitlab.com/programmer-assistant/addon-prometheus/commit/9318f32ba31ce965bf5ea406f4de58bf38374acf))

## 0.1.5

* Update prometheus to 2.30.0

## 0.1.4

* Update ingress icon

## 0.1.3

* Update ingress icon
* Update docs

## 0.1.2

* Fix token environment variable
* Add file configs

## 0.1.1

* Fix token environment variable

## 0.1.0

* Initial prometheus build based on 2.26.0

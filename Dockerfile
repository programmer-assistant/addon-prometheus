ARG DEP_PROXY=
FROM ${DEP_PROXY}programmerassistant/base:3.14

# Copy data
COPY rootfs /

ARG TARGETARCH
ARG TARGETVARIANT
ENV PROMETHEUS_VERSION "2.33.1"
# Setup base
RUN wget -q "https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VERSION}/prometheus-${PROMETHEUS_VERSION}.linux-${TARGETARCH}${TARGETVARIANT}.tar.gz" -O /tmp/prometheus.tar.gz \
    && mkdir -p /usr/src/prometheus \
    && tar -xf /tmp/prometheus.tar.gz --strip 1 -C /usr/src/prometheus \
    && ln -s /usr/src/prometheus/prometheus /usr/local/bin/prometheus \
    && rm -f /tmp/prometheus.tar.gz \
    && mkdir -p /etc/prometheus \
    && chown -R nobody:nobody /etc/prometheus /usr/src/prometheus


# Build arguments
ARG BUILD_DATE
ARG BUILD_DESCRIPTION
ARG BUILD_NAME
ARG BUILD_REF
ARG BUILD_VERSION

# Labels
LABEL \
    io.hass.name="${BUILD_NAME}" \
    io.hass.description="${BUILD_DESCRIPTION}" \
    io.hass.arch="multiarch" \
    io.hass.type="addon" \
    io.hass.version=${BUILD_VERSION} \
    maintainer="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.title="${BUILD_NAME}" \
    org.opencontainers.image.description="${BUILD_DESCRIPTION}" \
    org.opencontainers.image.vendor="Programmer Assistant HASS Add-on" \
    org.opencontainers.image.authors="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.url="https://addons.programmer-assistant.io" \
    org.opencontainers.image.source="https://gitlab.com/programmer-assistant/addons/-/tree/master/${BUILD_NAME}" \
    org.opencontainers.image.documentation="https://gitlab.com/programmer-assistant/addons/-/blob/master/${BUILD_NAME}/README.md" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.version=${BUILD_VERSION}
